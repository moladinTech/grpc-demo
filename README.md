gRPC Demo
=========

Persiapan
---------

Untuk menjalankan demo versi NodeJS, pastikan NodeJS
**versi 14.19.1 atau lebih baru** terpasang di mesin.

Untuk menjalankan demo versi Go, pastikan Go **versi 1.17 atau lebih baru**
terpasang di mesin.

[Protocol Buffer Compiler](https://github.com/protocolbuffers/protobuf/releases)
tidak harus ada untuk menjalankan client/server, namun dibutuhkan bila developer
ingin mencoba mengubah kontrak `.proto` tersedia.

Untuk membuka proyek ini, direkomendasikan menggunakan _Visual Studio Code_ atau
_Sublime Text_, karena proyek ini berisi bahasa JavaScript & Golang.

Menjalankan Server
------------------

Jalankan salah satu _server_ (port `50051` akan dipakai):

- versi NodeJS: pastikan `npm install` sudah pernah dijalankan,
  lalu jalankan `npm run startServer`
- versi Go: `go run go_server/main.go`

Menjalankan Client
------------------

Jalankan salah satu _client_ (port `8080` akan dipakai):

- versi NodeJS: pastikan `npm install` sudah pernah dijalankan,
  lalu jalankan `npm run startClient`
- versi Go: `go run go_client/main.go`

lalu gunakan dokumentasi REST di `_api/client.openapi.yml` untuk mengakses klien
agar bisa berkomunikasi dengan server via gRPC.

File API tersebut bisa dibuka menggunakan Postman, atau _copy_ isi file ke
https://editor.swagger.io/.

Compile `.proto`
----------------

Untuk meng-_compile_ file-file `.proto` menjadi `.js`, pastikan `npm install`
sudah dijalankan, lalu gunakan perintah berikut:
```sh
./node_modules/.bin/grpc_tools_node_protoc --proto_path=_proto/ \
  --plugin=protoc-gen-ts=./node_modules/.bin/protoc-gen-ts \
  --js_out=import_style=commonjs,binary:js_grpc \
  --grpc_out=grpc_js:js_grpc \
  --ts_out=js_grpc \
  _proto/wallet.proto
```

Untuk meng-_compile_ file-file `.proto` menjadi `.go`, pastikan `protoc-gen-go`
dan `protoc-gen-go-grpc` sudah terpasang (lihat
[RFC](https://moladin.atlassian.net/wiki/spaces/TEC/pages/307757085/Riset+Implementasi+gRPC+untuk+Microservice+Moladin#Dasar-Penggunaan-gRPC)
untuk detail instalasi), lalu gunakan perintah berikut:
```sh
protoc --proto_path=_proto/ \
  --go_out=go_grpc \
  --go-grpc_out=go_grpc \
  _proto/wallet.proto
```

Untuk meng-_compile_ `.proto` menjadi `.js` dan `.go` bersamaan, pastikan
syarat-syarat di bagian sebelumnya sudah terpenuhi, lalu jalankan:
```sh
./node_modules/.bin/grpc_tools_node_protoc --proto_path=_proto/ \
  --plugin=protoc-gen-ts=./node_modules/.bin/protoc-gen-ts \
  --js_out=import_style=commonjs,binary:js_grpc \
  --grpc_out=grpc_js:js_grpc \
  --ts_out=js_grpc \
  --go_out=. \
  --go-grpc_out=. \
  _proto/wallet.proto
```
