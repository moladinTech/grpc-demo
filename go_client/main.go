package main

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"time"

	"bitbucket.org/moladinTech/grpc-demo/go_client/webhandler"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

////////// STRUCT

// Pesan error yang dapat dibentuk menjadi JSON.
type errorMessage struct {
	ErrorCode    string `json:"errorCode"`
	ErrorMessage string `json:"errorMessage"`
}

////////// CONSTANT
var GRPC_ERR_CODE = map[codes.Code]string{
	codes.OK:                 "OK",
	codes.Canceled:           "CANCELLED",
	codes.Unknown:            "UNKNOWN",
	codes.InvalidArgument:    "INVALID_ARGUMENT",
	codes.DeadlineExceeded:   "DEADLINE_EXCEEDED",
	codes.NotFound:           "NOT_FOUND",
	codes.AlreadyExists:      "ALREADY_EXISTS",
	codes.PermissionDenied:   "PERMISSION_DENIED",
	codes.ResourceExhausted:  "RESOURCE_EXHAUSTED",
	codes.FailedPrecondition: "FAILED_PRECONDITION",
	codes.Aborted:            "ABORTED",
	codes.OutOfRange:         "OUT_OF_RANGE",
	codes.Unimplemented:      "UNIMPLEMENTED",
	codes.Internal:           "INTERNAL",
	codes.Unavailable:        "UNAVAILABLE",
	codes.DataLoss:           "DATA_LOSS",
	codes.Unauthenticated:    "UNAUTHENTICATED",
}

////////// MAIN
func main() {
	signalChan := make(chan os.Signal, 1)
	wg := sync.WaitGroup{}

	signal.Notify(signalChan, os.Interrupt)

	// Setup webserver
	e := echo.New()
	e.Logger.SetLevel(log.DEBUG)
	e.Use(middleware.CORS())
	e.HTTPErrorHandler = func(err error, ctx echo.Context) {
		var httpErr *echo.HTTPError
		if errors.As(err, &httpErr) {
			// Error dari Echo
			ctx.JSON(httpErr.Code, errorMessage{
				ErrorCode:    "-",
				ErrorMessage: httpErr.Error(),
			})
			return
		} else if grpcStatus, ok := status.FromError(err); ok {
			// Error dari gRPC
			// Untuk kebutuhan demo, ubah kode error gRPC menjadi teks
			// Normalnya, kode yang memanggil service RPC yang harus bertanggung jawab untuk
			//  menangkap error dan cek detailnya dengan `google.golang.org/grpc/status.FromError()`,
			//  lalu melakukan error handling sesuai flow bisnis.
			ctx.JSON(http.StatusBadRequest, errorMessage{
				ErrorCode:    GRPC_ERR_CODE[grpcStatus.Code()],
				ErrorMessage: grpcStatus.Message(),
			})
			return
		}

		// Error dari sistem
		ctx.JSON(http.StatusInternalServerError, errorMessage{
			ErrorCode:    "-",
			ErrorMessage: err.Error(),
		})
	}

	e.POST("/:username", webhandler.ActivateAccount)
	e.GET("/:username", webhandler.GetBalance)
	e.POST("/:username/deposit", webhandler.Deposit)
	e.POST("/:username/withdraw", webhandler.Withdraw)
	e.POST("/:username/transfer", webhandler.Transfer)

	// Setup teardown
	wg.Add(1)
	go func() {
		defer wg.Done()

		// Tunggu hingga ada signal yang diterima, lalu matikan server web
		<-signalChan

		shutdownCtx, cancel := context.WithTimeout(context.Background(), time.Second*5)
		defer cancel()

		e.Logger.Info("Shutting down...")
		if err := e.Shutdown(shutdownCtx); err != nil {
			e.Logger.Fatal("Cannot shutdown server gracefully:", err)
		}
	}()

	// Jalankan server
	if err := e.Start(":8080"); err != nil && err != http.ErrServerClosed {
		e.Logger.Fatal("Server is shut down with error: ", err)
	}

	// Pastikan semua goroutine sudah selesai
	wg.Wait()
}
