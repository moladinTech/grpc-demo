// Package service menyimpan logika bisnis.
// Untuk demo ini, logika hanya me-redirect request ke gRPC.
package service

import (
	"context"
	"log"
	"time"

	"bitbucket.org/moladinTech/grpc-demo/go_grpc"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

var walletClient go_grpc.WalletClient

func init() {
	// Sebelum service mengakses executor RPC, siapkan koneksi ke server
	conn, err := grpc.Dial("localhost:50051", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		panic(err)
	}

	walletClient = go_grpc.NewWalletClient(conn)
}

func ActivateAccount(username string) error {
	log.Println("CLIENT: mengatifkan akun", username)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	_, err := walletClient.ActivateAccount(ctx, &go_grpc.ActivateAccountRequest{
		Username: username,
	})
	if err != nil {
		return err
	}
	return nil
}

func GetBalance(username string) (int32, error) {
	log.Println("CLIENT:", username, "mencari saldo walletnya")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	rpcResp, err := walletClient.GetBalance(ctx, &go_grpc.GetBalanceRequest{
		Username: username,
	})
	if err != nil {
		return 0, err
	}
	return rpcResp.Balance, nil
}

func Deposit(username string, amount int32) (int32, error) {
	log.Println("CLIENT:", username, "menyetor", amount, "ke walletnya")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	rpcResp, err := walletClient.Deposit(ctx, &go_grpc.DepositRequest{
		Username: username,
		Amount:   amount,
	})
	if err != nil {
		return 0, err
	}
	return rpcResp.Balance, nil
}

func Withdraw(username string, amount int32) (int32, error) {
	log.Println("CLIENT:", username, "menarik", amount, "dari walletnya")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	rpcResp, err := walletClient.Withdraw(ctx, &go_grpc.WithdrawalRequest{
		Username: username,
		Amount:   amount,
	})
	if err != nil {
		return 0, err
	}
	return rpcResp.Balance, nil
}

func Transfer(senderUsername string, recipientUsername string, amount int32) (int32, int32, error) {
	log.Println("CLIENT: transfer", amount, "dari", senderUsername, "ke", recipientUsername)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	rpcResp, err := walletClient.Transfer(ctx, &go_grpc.TransferRequest{
		SenderUsername:    senderUsername,
		RecipientUsername: recipientUsername,
		Amount:            amount,
	})
	if err != nil {
		return 0, 0, err
	}
	return rpcResp.SenderBalance, rpcResp.RecipientBalance, nil
}
