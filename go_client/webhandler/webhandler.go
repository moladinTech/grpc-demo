// Package webhandler menyimpan semua handler function untuk server HTTP.
// Handler berguna untuk mengubah request web menjadi eksekusi logika bisnis,
// dan mengubah hasil eksekusi menjadi respon web.
package webhandler

import (
	"net/http"

	"bitbucket.org/moladinTech/grpc-demo/go_client/service"
	"github.com/labstack/echo/v4"
)

////////// STRUCT

type emptyResponse struct{}

type balanceResponse struct {
	Balance int32 `json:"balance"`
}

type amountRequest struct {
	Amount int32 `json:"amount"`
}

type transferRequest struct {
	RecipientUsername string `json:"recipientUsername"`
	Amount            int32  `json:"amount"`
}

type transferResponse struct {
	SenderBalance    int32 `json:"senderBalance"`
	RecipientBalance int32 `json:"recipientBalance"`
}

////////// FUNCTION

func ActivateAccount(c echo.Context) error {
	if err := service.ActivateAccount(c.Param("username")); err != nil {
		return err
	}

	return c.JSON(http.StatusCreated, emptyResponse{})
}

func GetBalance(c echo.Context) error {
	balance, err := service.GetBalance(c.Param("username"))
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, balanceResponse{
		Balance: balance,
	})
}

func Deposit(c echo.Context) error {
	request := amountRequest{}
	err := c.Bind(&request)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	balance, err := service.Deposit(c.Param("username"), request.Amount)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, balanceResponse{
		Balance: balance,
	})
}

func Withdraw(c echo.Context) error {
	request := amountRequest{}
	err := c.Bind(&request)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	balance, err := service.Withdraw(c.Param("username"), request.Amount)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, balanceResponse{
		Balance: balance,
	})
}

func Transfer(c echo.Context) error {
	request := transferRequest{}
	err := c.Bind(&request)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	senderBalance, recipientBalance, err := service.Transfer(c.Param("username"), request.RecipientUsername, request.Amount)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, transferResponse{
		SenderBalance:    senderBalance,
		RecipientBalance: recipientBalance,
	})
}
