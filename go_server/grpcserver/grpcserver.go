// Package grpcserver menyimpan semua implementasi server gRPC untuk app ini.
//
// Kelas-kelas yang ada dibawah package ini bertugas untuk menerjemahkan
// request gRPC menjadi eksekusi service layer, dan mengubah kembali hasil eksekusi
// service menjadi response gRPC.
package grpcserver
