package grpcserver

import (
	"context"

	"bitbucket.org/moladinTech/grpc-demo/go_grpc"
	"bitbucket.org/moladinTech/grpc-demo/go_server/service"
)

////////// FUNCTION

// NewWalletServer membuat server wallet yang baru.
//
// Implements `go_grpc.WalletServer`.
func NewWalletServer() *walletServerImpl {
	return &walletServerImpl{
		walletService: service.NewWalletService(),
	}
}

////////// STRUCT & METHOD

type walletServerImpl struct {
	go_grpc.UnimplementedWalletServer

	walletService service.WalletService
}

func (server *walletServerImpl) ActivateAccount(ctx context.Context, req *go_grpc.ActivateAccountRequest) (*go_grpc.ActivateAccountResponse, error) {
	err := server.walletService.ActivateAccount(req.Username)
	if err != nil {
		return nil, err
	}

	return &go_grpc.ActivateAccountResponse{}, nil
}

func (server *walletServerImpl) GetBalance(ctx context.Context, req *go_grpc.GetBalanceRequest) (*go_grpc.GetBalanceResponse, error) {
	balance, err := server.walletService.GetBalance(req.Username)
	if err != nil {
		return nil, err
	}

	return &go_grpc.GetBalanceResponse{
		Balance: int32(balance),
	}, nil
}

func (server *walletServerImpl) Deposit(ctx context.Context, req *go_grpc.DepositRequest) (*go_grpc.DepositResponse, error) {
	balance, err := server.walletService.Deposit(req.Username, req.Amount)
	if err != nil {
		return nil, err
	}

	return &go_grpc.DepositResponse{
		Balance: int32(balance),
	}, nil
}

func (server *walletServerImpl) Withdraw(ctx context.Context, req *go_grpc.WithdrawalRequest) (*go_grpc.WithdrawalResponse, error) {
	balance, err := server.walletService.Withdraw(req.Username, req.Amount)
	if err != nil {
		return nil, err
	}

	return &go_grpc.WithdrawalResponse{
		Balance: int32(balance),
	}, nil
}

func (server *walletServerImpl) Transfer(ctx context.Context, req *go_grpc.TransferRequest) (*go_grpc.TransferResponse, error) {
	newSenderBalance, newRecipientBalance, err := server.walletService.Transfer(req.SenderUsername, req.RecipientUsername, req.Amount)
	if err != nil {
		return nil, err
	}

	return &go_grpc.TransferResponse{
		SenderBalance:    int32(newSenderBalance),
		RecipientBalance: int32(newRecipientBalance),
	}, nil
}
