package main

import (
	"log"
	"net"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"bitbucket.org/moladinTech/grpc-demo/go_grpc"
	"bitbucket.org/moladinTech/grpc-demo/go_server/grpcserver"
	"google.golang.org/grpc"
)

var grpcServer *grpc.Server

func main() {
	// Buka port untuk server
	listener, err := net.Listen("tcp", "localhost:50051")
	if err != nil {
		log.Fatalln("Tidak bisa membuka port 50051")
	}

	wg := sync.WaitGroup{}
	wg.Add(1)

	// Deteksi sinyal untuk mematikan aplikasi
	go func() {
		signalChan := make(chan os.Signal, 1)
		signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

		// Ketika sinyal diterima, maka matikan server gRPC
		<-signalChan
		grpcServer.GracefulStop()

		close(signalChan)
		wg.Done()
	}()

	// Buat & setup server gRPC
	grpcServer = grpc.NewServer()
	go_grpc.RegisterWalletServer(grpcServer, grpcserver.NewWalletServer())

	grpcServer.Serve(listener)

	// Server dimatikan secara graceful. Tunggu hingga proses server benar-benar selesai
	wg.Wait()
}
