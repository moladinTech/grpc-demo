// Package servererror menyimpan semua error khusus server yang dapat diubah menjadi
// status error gRPC.
package servererror

import (
	"fmt"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

////////// VARIABLES
var (
	// InvalidValueError adalah error ketika ada data yang tidak sesuai seharusnya.
	InvalidValueError = &serverError{
		message:  "invalid value",
		grpcCode: codes.InvalidArgument,
	}

	// NegativeValueError adalah error ketika ada angka yang negatif.
	NegativeValueError = &serverError{
		message:  "value cannot be negative",
		grpcCode: codes.InvalidArgument,
	}

	// UsernameNotExistError adalah error ketika username wallet belum terdaftar di sistem.
	UsernameNotExistError = &serverError{
		message:  "username does not exist",
		grpcCode: codes.NotFound,
	}

	// UsernameExistError adalah error ketika username wallet sudah terdaftar di sistem.
	UsernameExistError = &serverError{
		message:  "username already exists",
		grpcCode: codes.AlreadyExists,
	}

	// InsufficientBalanceError adalah error ketika saldo tidak mencukupi.
	InsufficientBalanceError = &serverError{
		message:  "insufficient balance",
		grpcCode: codes.FailedPrecondition,
	}
)

////////// STRUCT & METHOD

// serverError merupakan kelas error yang menyimpan pesan error umum &
// info kode error gRPC, tergantung tipe error.
type serverError struct {
	// Pesan untuk tipe error ini.
	message string

	// Kode error gRPC yang terkait dengan tipe error ini.
	grpcCode codes.Code
}

func (err *serverError) Error() string {
	return err.message
}

type grpcCompatibleError struct {
	// Tipe error.
	errorType *serverError

	// Pesan tambahan untuk spesifik error ini.
	message string
}

func (err *grpcCompatibleError) Error() string {
	return fmt.Sprintf("%s: %s", err.errorType.message, err.message)
}

func (err *grpcCompatibleError) Unwrap() error {
	return err.errorType
}

// GRPCStatus mengubah error server menjadi status error yang bisa dikirim lewat gRPC.
func (err *grpcCompatibleError) GRPCStatus() *status.Status {
	return status.New(err.errorType.grpcCode, err.Error())
}

////////// FUNCTION

// NewInvalidValueError mengeluarkan error karena ada data yang tidak sesuai seharusnya.
func NewInvalidValueError(message string) *grpcCompatibleError {
	return &grpcCompatibleError{
		errorType: InvalidValueError,
		message:   message,
	}
}

// NewNegativeValueError mengeluarkan error ketika ada angka yang negatif.
func NewNegativeValueError(paramName string, value int64) *grpcCompatibleError {
	return &grpcCompatibleError{
		errorType: NegativeValueError,
		message:   fmt.Sprintf("[%s = %d]", paramName, value),
	}
}

// NewUsernameNotExistError mengeluarkan error karena username wallet belum terdaftar di sistem.
func NewUsernameNotExistError(username string) *grpcCompatibleError {
	return &grpcCompatibleError{
		errorType: UsernameNotExistError,
		message:   fmt.Sprintf("'%s'", username),
	}
}

// NewUsernameExistError mengeluarkan error karena username wallet sudah terdaftar di sistem.
func NewUsernameExistError(username string) *grpcCompatibleError {
	return &grpcCompatibleError{
		errorType: UsernameExistError,
		message:   fmt.Sprintf("'%s'", username),
	}
}

// NewInsufficientBalanceError mengeluarkan error ketika saldo tidak mencukupi.
func NewInsufficientBalanceError(consumedAmount, remainingAmount int) *grpcCompatibleError {
	return &grpcCompatibleError{
		errorType: InsufficientBalanceError,
		message:   fmt.Sprintf("about to consume %d, with remaining balance %d", consumedAmount, remainingAmount),
	}
}
