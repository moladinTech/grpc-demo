package service

import (
	"log"
	"strings"
	"sync"

	"bitbucket.org/moladinTech/grpc-demo/go_server/servererror"
)

////////// INTERFACE

// WalletService interface untuk service (logika bisnis) wallet.
type WalletService interface {
	// ActivateAccount mengaktifkan akun untuk username tertentu.
	ActivateAccount(username string) error

	// GetBalance mengeluarkan info saldo user.
	GetBalance(username string) (int, error)

	// Deposit menambah saldo wallet 1 user.
	Deposit(username string, amount int32) (int, error)

	// Withdraw mengurangi saldo wallet 1 user.
	Withdraw(username string, amount int32) (int, error)

	// Transfer memindahkan saldo wallet dari 1 user ke user lain.
	//
	// Mengeluarkan balance pengirim & penerima setelah proses transfer selesai.
	Transfer(senderUsername, recipientUsername string, amount int32) (int, int, error)
}

////////// FUNCTION

// NewWalletService membuat service wallet baru.
//
// Implements `service.WalletService`
func NewWalletService() *walletServiceImpl {
	return &walletServiceImpl{
		walletData: make(map[string]int),
	}
}

////////// STRUCT & METHOD

//
type walletServiceImpl struct {
	// Tempat penyimpanan (dummy) data wallet.
	walletData map[string]int

	// Mutex untuk mengamankan akses data wallet
	walletMutex sync.Mutex
}

func (service *walletServiceImpl) ActivateAccount(username string) error {
	log.Println("SERVER: mengatifkan akun", username)

	if username == "" {
		return servererror.NewInvalidValueError("username cannot be empty")
	}

	// Pastikan user belum terdaftar
	lowercaseUsername := strings.ToLower(username)
	_, ok := service.walletData[lowercaseUsername]
	if ok {
		return servererror.NewUsernameExistError(username)
	}

	// Buat akun baru
	service.walletMutex.Lock()
	defer service.walletMutex.Unlock()

	service.walletData[lowercaseUsername] = 0
	return nil
}

func (service *walletServiceImpl) GetBalance(username string) (int, error) {
	log.Println("SERVER:", username, "mencari saldo walletnya")

	if username == "" {
		return 0, servererror.NewInvalidValueError("username cannot be empty")
	}

	// Pastikan user sudah terdaftar
	lowercaseUsername := strings.ToLower(username)
	_, ok := service.walletData[lowercaseUsername]
	if !ok {
		return 0, servererror.NewUsernameNotExistError(username)
	}

	// Keluarkan saldo user
	service.walletMutex.Lock()
	defer service.walletMutex.Unlock()

	return service.walletData[lowercaseUsername], nil
}

func (service *walletServiceImpl) Deposit(username string, amount int32) (int, error) {
	log.Println("SERVER:", username, "menyetor", amount, "ke walletnya")

	if username == "" {
		return 0, servererror.NewInvalidValueError("username cannot be empty")
	}

	if amount < 0 {
		return 0, servererror.NewNegativeValueError("amount", int64(amount))
	}

	// Pastikan user sudah terdaftar
	lowercaseUsername := strings.ToLower(username)
	_, ok := service.walletData[lowercaseUsername]
	if !ok {
		return 0, servererror.NewUsernameNotExistError(username)
	}

	// Tambahkan saldo user
	service.walletMutex.Lock()
	defer service.walletMutex.Unlock()

	service.walletData[lowercaseUsername] += int(amount)
	return service.walletData[lowercaseUsername], nil
}

func (service *walletServiceImpl) Withdraw(username string, amount int32) (int, error) {
	log.Println("SERVER:", username, "menarik", amount, "dari walletnya")

	if username == "" {
		return 0, servererror.NewInvalidValueError("username cannot be empty")
	}

	if amount < 0 {
		return 0, servererror.NewNegativeValueError("amount", int64(amount))
	}

	// Pastikan user sudah terdaftar
	lowercaseUsername := strings.ToLower(username)
	_, ok := service.walletData[lowercaseUsername]
	if !ok {
		return 0, servererror.NewUsernameNotExistError(username)
	}

	// Cek bila saldo user mencukupi
	service.walletMutex.Lock()
	defer service.walletMutex.Unlock()

	amountInt := int(amount)
	curBalance := service.walletData[lowercaseUsername]
	if curBalance < amountInt {
		return 0, servererror.NewInsufficientBalanceError(amountInt, curBalance)
	}

	service.walletData[lowercaseUsername] -= amountInt
	return service.walletData[lowercaseUsername], nil
}

func (service *walletServiceImpl) Transfer(senderUsername, recipientUsername string, amount int32) (int, int, error) {
	log.Println("SERVER: transfer", amount, "dari", senderUsername, "ke", recipientUsername)

	if senderUsername == "" {
		return 0, 0, servererror.NewInvalidValueError("sender username cannot be empty")
	}
	if recipientUsername == "" {
		return 0, 0, servererror.NewInvalidValueError("recipient username cannot be empty")
	}

	if amount < 0 {
		return 0, 0, servererror.NewNegativeValueError("amount", int64(amount))
	}

	// Pastikan user sudah terdaftar
	lowercaseSenderUsername := strings.ToLower(senderUsername)
	_, ok := service.walletData[lowercaseSenderUsername]
	if !ok {
		return 0, 0, servererror.NewUsernameNotExistError(senderUsername)
	}

	lowercaseRecipientUsername := strings.ToLower(recipientUsername)
	_, ok = service.walletData[lowercaseRecipientUsername]
	if !ok {
		return 0, 0, servererror.NewUsernameNotExistError(recipientUsername)
	}

	// Cek bila saldo user mencukupi
	service.walletMutex.Lock()
	defer service.walletMutex.Unlock()

	amountInt := int(amount)
	curSenderBalance := service.walletData[lowercaseSenderUsername]
	if curSenderBalance < amountInt {
		return 0, 0, servererror.NewInsufficientBalanceError(amountInt, curSenderBalance)
	}

	// Pindahkan saldo
	service.walletData[lowercaseSenderUsername] -= amountInt
	service.walletData[lowercaseRecipientUsername] += amountInt
	return service.walletData[lowercaseSenderUsername], service.walletData[lowercaseRecipientUsername], nil
}
