package service_test

import (
	"fmt"
	"testing"

	"bitbucket.org/moladinTech/grpc-demo/go_server/servererror"
	"bitbucket.org/moladinTech/grpc-demo/go_server/service"
	"github.com/stretchr/testify/require"
)

////////// ActivateAccount

func TestActivateAccount_ShouldSucceed(t *testing.T) {
	// jika
	tested := service.NewWalletService()
	username := "username"

	// ketika
	err := tested.ActivateAccount(username)

	// maka
	require.NoError(t, err)

	balance, err := tested.GetBalance(username)
	require.NoError(t, err)
	require.Equal(t, 0, balance)
}

func TestActivateAccount_ErrorOnDuplicateUsername(t *testing.T) {
	testTable := []struct {
		username          string
		duplicateUsername string
	}{{
		username:          "username",
		duplicateUsername: "username",
	}, {
		username:          "akun_KAPITAL",
		duplicateUsername: "aKuN_KaPiTaL",
	}}

	for _, test := range testTable {
		username := test.username
		duplicateUsername := test.duplicateUsername

		t.Run(fmt.Sprintf("error dobel username untuk '%s' vs '%s'", username, duplicateUsername), func(t *testing.T) {
			// jika
			tested := service.NewWalletService()
			tested.ActivateAccount(username)

			// ketika
			err := tested.ActivateAccount(duplicateUsername)

			// maka
			require.ErrorIs(t, err, servererror.UsernameExistError)
		})
	}
}

func TestActivateAccount_ErrorOnEmptyUsername(t *testing.T) {
	// jika
	tested := service.NewWalletService()

	// ketika
	err := tested.ActivateAccount("")

	// maka
	require.ErrorIs(t, err, servererror.InvalidValueError)
}

////////// GetBalance

func TestGetBalance_Succeeds(t *testing.T) {
	testCases := []struct {
		activateUsername   string
		getBalanceUsername string
	}{{
		activateUsername:   "username",
		getBalanceUsername: "username",
	}, {
		activateUsername:   "akun_KAPITAL",
		getBalanceUsername: "aKuN_KaPiTaL",
	}}

	for _, test := range testCases {
		t.Run(fmt.Sprintf("berhasil untuk user '%s'", test.getBalanceUsername), func(t *testing.T) {
			// jika
			tested := service.NewWalletService()
			tested.ActivateAccount(test.activateUsername)

			// ketika
			balance, err := tested.GetBalance(test.getBalanceUsername)

			// maka
			require.NoError(t, err)
			require.Equal(t, 0, balance)
		})
	}
}

func TestGetBalance_ErrorWhenUsernameNotExist(t *testing.T) {
	// jika
	tested := service.NewWalletService()

	// ketika
	_, err := tested.GetBalance("invalid")

	// maka
	require.ErrorIs(t, err, servererror.UsernameNotExistError)
}

func TestGetBalance_ErrorWhenUsernameIsEmpty(t *testing.T) {
	// jika
	tested := service.NewWalletService()

	// ketika
	_, err := tested.GetBalance("")

	// maka
	require.ErrorIs(t, err, servererror.InvalidValueError)
}

////////// Deposit

func TestDeposit_Succeeds(t *testing.T) {
	testCases := []struct {
		activateUsername string
		depositUsername  string
		amount           int
	}{{
		activateUsername: "username",
		depositUsername:  "username",
		amount:           444,
	}, {
		activateUsername: "akun_KAPITAL",
		depositUsername:  "aKuN_KaPiTaL",
		amount:           777,
	}, {
		activateUsername: "zero",
		depositUsername:  "zero",
		amount:           0, // Tetap berhasil walaupun jumlah setoran 0
	}}

	for _, test := range testCases {
		t.Run(fmt.Sprintf("berhasil untuk user '%s'", test.depositUsername), func(t *testing.T) {
			// jika
			tested := service.NewWalletService()
			tested.ActivateAccount(test.activateUsername)

			// ketika
			newBalance, err := tested.Deposit(test.depositUsername, int32(test.amount))

			// maka
			require.NoError(t, err)
			require.Equal(t, test.amount, newBalance)
		})
	}
}

func TestDeposit_ErrorWhenUsernameNotExist(t *testing.T) {
	// jika
	tested := service.NewWalletService()

	// ketika
	balance, err := tested.Deposit("invalid", 555)

	// maka
	require.ErrorIs(t, err, servererror.UsernameNotExistError)
	require.Equal(t, 0, balance)
}

func TestDeposit_ErrorWhenUsernameIsEmpty(t *testing.T) {
	// jika
	tested := service.NewWalletService()

	// ketika
	balance, err := tested.Deposit("", 111)

	// maka
	require.ErrorIs(t, err, servererror.InvalidValueError)
	require.Equal(t, 0, balance)
}

func TestDeposit_ErrorWhenAmountIsNegative(t *testing.T) {
	// jika
	tested := service.NewWalletService()

	username := "username"
	tested.Deposit(username, int32(951))

	// ketika
	balance, err := tested.Deposit(username, -1)

	// maka
	require.ErrorIs(t, err, servererror.NegativeValueError)
	require.Equal(t, 0, balance) // Saldo original user tidak perlu ditampilkan
}

////////// Withdraw

func TestWithdraw_Succeeds(t *testing.T) {
	testCases := []struct {
		activateUsername   string
		withdrawalUsername string
		initAmount         int
		withdrawalAmount   int
		remainingBalance   int
	}{{
		activateUsername:   "username",
		withdrawalUsername: "username",
		initAmount:         555,
		withdrawalAmount:   555,
		remainingBalance:   0,
	}, {
		activateUsername:   "akun_KAPITAL",
		withdrawalUsername: "aKuN_KaPiTaL",
		initAmount:         888,
		withdrawalAmount:   262,
		remainingBalance:   626,
	}, {
		activateUsername:   "zero",
		withdrawalUsername: "zero",
		initAmount:         456,
		withdrawalAmount:   0, // Tetap berhasil walaupun jumlah penarikan 0
		remainingBalance:   456,
	}}

	for _, test := range testCases {
		t.Run(fmt.Sprintf("berhasil untuk user '%s'", test.withdrawalUsername), func(t *testing.T) {
			// jika
			tested := service.NewWalletService()
			tested.ActivateAccount(test.activateUsername)
			tested.Deposit(test.withdrawalUsername, int32(test.initAmount))

			// ketika
			newBalance, err := tested.Withdraw(test.withdrawalUsername, int32(test.withdrawalAmount))

			// maka
			require.NoError(t, err)
			require.Equal(t, test.remainingBalance, newBalance)
		})
	}
}

func TestWithdraw_ErrorWhenUsernameNotExist(t *testing.T) {
	// jika
	tested := service.NewWalletService()

	// ketika
	balance, err := tested.Withdraw("invalid", 555)

	// maka
	require.ErrorIs(t, err, servererror.UsernameNotExistError)
	require.Equal(t, 0, balance)
}

func TestWithdraw_ErrorWhenUsernameIsEmpty(t *testing.T) {
	// jika
	tested := service.NewWalletService()

	// ketika
	balance, err := tested.Withdraw("", 111)

	// maka
	require.ErrorIs(t, err, servererror.InvalidValueError)
	require.Equal(t, 0, balance)
}

func TestWithdraw_ErrorWhenAmountIsNegative(t *testing.T) {
	// jika
	tested := service.NewWalletService()

	username := "username"
	tested.ActivateAccount(username)
	tested.Deposit(username, int32(951))

	// ketika
	balance, err := tested.Withdraw(username, -1)

	// maka
	require.ErrorIs(t, err, servererror.NegativeValueError)
	require.Equal(t, 0, balance) // Saldo original user tidak perlu ditampilkan
}

func TestWithdraw_ErrorWhenBalanceIsInsufficient(t *testing.T) {
	testCases := []struct {
		initAmount       int
		withdrawalAmount int
	}{{
		initAmount:       0,
		withdrawalAmount: 123,
	}, {
		initAmount:       123,
		withdrawalAmount: 999,
	}}

	for _, test := range testCases {
		t.Run(fmt.Sprintf("gagal ketika penarikan %d, dengan sisa saldo %d", test.withdrawalAmount, test.initAmount), func(t *testing.T) {
			// jika
			tested := service.NewWalletService()
			username := "username"

			tested.ActivateAccount(username)
			tested.Deposit(username, int32(test.initAmount))

			// ketika
			balance, err := tested.Withdraw(username, int32(test.withdrawalAmount))

			// maka
			require.ErrorIs(t, err, servererror.InsufficientBalanceError)
			require.Equal(t, 0, balance) // Saldo original user tidak perlu ditampilkan
		})
	}
}

////////// TRANSFER

func TestTransfer_Succeeds(t *testing.T) {
	testCases := []struct {
		activateSenderUsername    string
		activateRecipientUsername string
		transferSenderUsername    string
		transferRecipientUsername string
		initAmount                int
		transferAmount            int
		remainingSenderBalance    int
	}{{
		activateSenderUsername:    "sender",
		activateRecipientUsername: "recipient",
		transferSenderUsername:    "sender",
		transferRecipientUsername: "recipient",
		initAmount:                458,
		transferAmount:            458,
		remainingSenderBalance:    0,
	}, {
		activateSenderUsername:    "sender",
		activateRecipientUsername: "recipient",
		transferSenderUsername:    "SENDER",
		transferRecipientUsername: "RECIPIENT",
		initAmount:                999,
		transferAmount:            654,
		remainingSenderBalance:    345,
	}}

	for _, test := range testCases {
		t.Run(fmt.Sprintf("berhasil transfer dari '%s' ke '%s' sebanyak %d", test.transferSenderUsername, test.transferRecipientUsername, test.transferAmount), func(t *testing.T) {
			// jika
			tested := service.NewWalletService()
			tested.ActivateAccount(test.activateSenderUsername)
			tested.ActivateAccount(test.activateRecipientUsername)
			tested.Deposit(test.activateSenderUsername, int32(test.initAmount))

			// ketika
			newSenderBalance, newRecipientBalance, err := tested.Transfer(test.transferSenderUsername, test.activateRecipientUsername, int32(test.transferAmount))

			// maka
			require.NoError(t, err)
			require.Equal(t, test.remainingSenderBalance, newSenderBalance)
			require.Equal(t, test.transferAmount, newRecipientBalance)
		})
	}
}

func TestTransfer_ErrorWhenUsernameNotExist(t *testing.T) {
	testCases := []struct {
		activateSenderUsername    string
		activateRecipientUsername string
		transferSenderUsername    string
		transferRecipientUsername string
		expectedUsernameOnError   string
		testTarget                string
	}{{
		activateSenderUsername:    "sender",
		activateRecipientUsername: "",
		transferSenderUsername:    "sender",
		transferRecipientUsername: "recipient",
		expectedUsernameOnError:   "recipient",
		testTarget:                "username penerima",
	}, {
		activateSenderUsername:    "",
		activateRecipientUsername: "recipient",
		transferSenderUsername:    "sender",
		transferRecipientUsername: "recipient",
		expectedUsernameOnError:   "sender",
		testTarget:                "username pengirim",
	}, {
		activateSenderUsername:    "",
		activateRecipientUsername: "",
		transferSenderUsername:    "sender",
		transferRecipientUsername: "recipient",
		expectedUsernameOnError:   "sender",
		testTarget:                "kedua username",
	}}

	for _, test := range testCases {
		t.Run(fmt.Sprintf("error ketika %s tidak terdaftar", test.testTarget), func(t *testing.T) {
			// jika
			tested := service.NewWalletService()

			if test.activateSenderUsername != "" {
				tested.ActivateAccount(test.activateSenderUsername)
			}
			if test.activateRecipientUsername != "" {
				tested.ActivateAccount(test.activateRecipientUsername)
			}

			// ketika
			newSenderBalance, newRecipientBalance, err := tested.Transfer(test.transferSenderUsername, test.transferRecipientUsername, int32(123))

			// maka
			require.ErrorIs(t, err, servererror.UsernameNotExistError)
			require.ErrorContains(t, err, test.expectedUsernameOnError)
			require.Equal(t, 0, newSenderBalance)
			require.Equal(t, 0, newRecipientBalance)
		})
	}
}

func TestTransfer_ErrorWhenUsernameIsEmpty(t *testing.T) {
	testCases := []struct {
		activateSenderUsername    string
		activateRecipientUsername string
		transferSenderUsername    string
		transferRecipientUsername string
		expectedUsernameOnError   string
		testTarget                string
	}{{
		activateSenderUsername:    "sender",
		activateRecipientUsername: "recipient",
		transferSenderUsername:    "sender",
		transferRecipientUsername: "",
		expectedUsernameOnError:   "recipient",
		testTarget:                "username penerima",
	}, {
		activateSenderUsername:    "sender",
		activateRecipientUsername: "recipient",
		transferSenderUsername:    "",
		transferRecipientUsername: "recipient",
		expectedUsernameOnError:   "sender",
		testTarget:                "username pengirim",
	}, {
		activateSenderUsername:    "sender",
		activateRecipientUsername: "recipient",
		transferSenderUsername:    "",
		transferRecipientUsername: "",
		expectedUsernameOnError:   "sender",
		testTarget:                "kedua username",
	}}

	for _, test := range testCases {
		t.Run(fmt.Sprintf("error ketika %s kosong", test.testTarget), func(t *testing.T) {
			// jika
			tested := service.NewWalletService()

			tested.ActivateAccount(test.activateSenderUsername)
			tested.ActivateAccount(test.activateRecipientUsername)

			// ketika
			newSenderBalance, newRecipientBalance, err := tested.Transfer(test.transferSenderUsername, test.transferRecipientUsername, int32(123))

			// maka
			require.ErrorIs(t, err, servererror.InvalidValueError)
			require.ErrorContains(t, err, test.expectedUsernameOnError)
			require.Equal(t, 0, newSenderBalance)
			require.Equal(t, 0, newRecipientBalance)
		})
	}
}

func TestTransfer_ErrorWhenAmountIsNegative(t *testing.T) {
	// jika
	tested := service.NewWalletService()

	senderUsername := "sender"
	recipientUsername := "sender"
	tested.ActivateAccount(senderUsername)
	tested.ActivateAccount(recipientUsername)
	tested.Deposit(senderUsername, int32(999))
	tested.Deposit(recipientUsername, int32(888))

	// ketika
	recipientSenderBalance, newRecipientBalance, err := tested.Transfer(senderUsername, recipientUsername, -1)

	// maka
	require.ErrorIs(t, err, servererror.NegativeValueError)
	// Saldo original user tidak perlu ditampilkan
	require.Equal(t, 0, recipientSenderBalance)
	require.Equal(t, 0, newRecipientBalance)
}

func TestTransfer_ErrorWhenBalanceIsInsufficient(t *testing.T) {
	testCases := []struct {
		initAmount     int
		transferAmount int
	}{{
		initAmount:     0,
		transferAmount: 123,
	}, {
		initAmount:     123,
		transferAmount: 999,
	}}

	for _, test := range testCases {
		t.Run(fmt.Sprintf("gagal ketika transfer %d, dengan sisa saldo %d", test.transferAmount, test.initAmount), func(t *testing.T) {
			// jika
			tested := service.NewWalletService()
			senderUsername := "sender"
			recipientUsername := "recipient"

			tested.ActivateAccount(senderUsername)
			tested.ActivateAccount(recipientUsername)
			tested.Deposit(senderUsername, int32(test.initAmount))

			// ketika
			newSenderBalance, newRecipientBalance, err := tested.Transfer(senderUsername, recipientUsername, int32(test.transferAmount))

			// maka
			require.ErrorIs(t, err, servererror.InsufficientBalanceError)
			// Saldo original user tidak perlu ditampilkan
			require.Equal(t, 0, newSenderBalance)
			require.Equal(t, 0, newRecipientBalance)
		})
	}
}
