const grpc = require('@grpc/grpc-js');
const { promisify } = require('util');

const { WalletClient } = require('../js_grpc/wallet_grpc_pb');
const {
    ActivateAccountRequest,
    GetBalanceRequest,
    GetBalanceResponse,
    DepositRequest,
    DepositResponse,
    WithdrawalRequest,
    WithdrawalResponse,
    TransferRequest,
    TransferResponse,
} = require('../js_grpc/wallet_pb');

/**
 * Kelas yang menyimpan logika bisnis _wallet_.
 *
 * Di sisi _client_, kelas ini menjadi _wrapper_ untuk eksekusi gRPC.
 */
module.exports = class WalletService {
    constructor() {
        this.walletRpc = new WalletClient(
            'localhost:50051',
            grpc.credentials.createInsecure()
        );
    }

    /**
     * Mengaktifkan akun untuk username tertentu.
     *
     * @param {string} username Nama user yang _wallet_-nya akan diaktifkan.
     *
     * @returns {Promise<void>} Hasil aktifasi akun.
     */
    async activateAccount(username) {
        console.log(`CLIENT: mengatifkan akun ${username}`);

        await promisify((callback) => {
            this.walletRpc.activateAccount(
                new ActivateAccountRequest().setUsername(username),
                callback
            );
        })();

        // Langsung keluar function, karena respon RPC tidak ada data
    }

    /**
     * Cari berapa saldo user saat ini.
     *
     * @param {string} username Nama user yang saldo _wallet_-nya dicari.
     *
     * @returns {Promise<number>} Integer saldo user ini.
     */
    async getBalance(username) {
        console.log(`CLIENT: ${username} mencari saldo walletnya`);

        /** @type {GetBalanceResponse} */
        const rpcResponse = await promisify((cb) => {
            this.walletRpc.getBalance(
                new GetBalanceRequest().setUsername(username),
                cb
            );
        })();

        return rpcResponse.getBalance();
    }

    /**
     * Menambah saldo _wallet_ 1 user.
     *
     * @param {string} username Nama user yang saldonya akan ditambah.
     * @param {number} amount Jumlah penambahan saldo.
     *
     * @returns {Promise<number>} Saldo user setelah penambahan saldo.
     */
    async deposit(username, amount) {
        console.log(`CLIENT: ${username} menyetor ${amount} ke walletnya`);

        /** @type {DepositResponse} */
        const rpcResponse = await promisify((cb) => {
            this.walletRpc.deposit(
                new DepositRequest().setUsername(username).setAmount(amount),
                cb
            );
        })();

        return rpcResponse.getBalance();
    }

    /**
     * Mengurangi saldo _wallet_ 1 user.
     *
     * @param {string} username Nama user yang saldonya akan dikurangi.
     * @param {number} amount Jumlah pengurangan saldo.
     *
     * @returns {Promise<number>} Saldo user setelah pengurangan saldo.
     */
    async withdraw(username, amount) {
        console.log(`CLIENT: ${username} menarik ${amount} dari walletnya`);

        /** @type {WithdrawalResponse} */
        const rpcResponse = await promisify((cb) => {
            this.walletRpc.withdraw(
                new WithdrawalRequest().setUsername(username).setAmount(amount),
                cb
            );
        })();

        return rpcResponse.getBalance();
    }

    /**
     * Memindahkan saldo _wallet_  dari 1 user ke user lain.
     *
     * @param {string} senderUsername Nama user pengirim transfer.
     * @param {string} recipientUsername Nama user penerima transfer.
     * @param {number} amount Jumlah saldo yang ditransfer.
     *
     * @returns {{senderBalance: number, recipientBalance: number}} Saldo kedua user setelah transfer selesai.
     */
    async transfer(senderUsername, recipientUsername, amount) {
        console.log(
            `CLIENT: transfer ${amount} dari ${senderUsername} ke ${recipientUsername}`
        );

        /** @type {TransferResponse} */
        const rpcResponse = await promisify((cb) => {
            this.walletRpc.transfer(
                new TransferRequest()
                    .setSenderUsername(senderUsername)
                    .setRecipientUsername(recipientUsername)
                    .setAmount(amount),
                cb
            );
        })();

        return {
            senderBalance: rpcResponse.getSenderBalance(),
            recipientBalance: rpcResponse.getRecipientBalance(),
        };
    }
};
