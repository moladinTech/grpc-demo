require('express-async-errors');
const express = require('express');
const cors = require('cors');
const grpc = require('@grpc/grpc-js');

const WalletService = require('./WalletService');
const GRPC_ERROR_CODE_MAP = {
    [grpc.status.CANCELLED]: 'CANCELLED',
    [grpc.status.UNKNOWN]: 'UNKNOWN',
    [grpc.status.INVALID_ARGUMENT]: 'INVALID_ARGUMENT',
    [grpc.status.DEADLINE_EXCEEDED]: 'DEADLINE_EXCEEDED',
    [grpc.status.NOT_FOUND]: 'NOT_FOUND',
    [grpc.status.ALREADY_EXISTS]: 'ALREADY_EXISTS',
    [grpc.status.PERMISSION_DENIED]: 'PERMISSION_DENIED',
    [grpc.status.RESOURCE_EXHAUSTED]: 'RESOURCE_EXHAUSTED',
    [grpc.status.FAILED_PRECONDITION]: 'FAILED_PRECONDITION',
    [grpc.status.ABORTED]: 'ABORTED',
    [grpc.status.OUT_OF_RANGE]: 'OUT_OF_RANGE',
    [grpc.status.UNIMPLEMENTED]: 'UNIMPLEMENTED',
    [grpc.status.INTERNAL]: 'INTERNAL',
    [grpc.status.UNAVAILABLE]: 'UNAVAILABLE',
    [grpc.status.DATA_LOSS]: 'DATA_LOSS',
    [grpc.status.UNAUTHENTICATED]: 'UNAUTHENTICATED',
};

const walletService = new WalletService();
const app = express();
app.use(cors());
app.use(express.json());

app.post('/:username', async (req, resp) => {
    await walletService.activateAccount(req.params.username);
    resp.status(201).json({});
});

app.get('/:username', async (req, resp) => {
    const balance = await walletService.getBalance(req.params.username);
    resp.status(200).json({
        balance,
    });
});

app.post('/:username/deposit', async (req, resp) => {
    const balance = await walletService.deposit(
        req.params.username,
        req.body.amount
    );
    resp.status(200).json({
        balance,
    });
});

app.post('/:username/withdraw', async (req, resp) => {
    const balance = await walletService.withdraw(
        req.params.username,
        req.body.amount
    );
    resp.status(200).json({
        balance,
    });
});

app.post('/:username/transfer', async (req, resp) => {
    const balanceAfterTransfer = await walletService.transfer(
        req.params.username,
        req.body.recipientUsername,
        req.body.amount
    );
    resp.status(200).json(balanceAfterTransfer);
});

// ~ Express error handling ~
app.use((err, _req, resp, next) => {
    // Untuk kebutuhan demo, ubah kode error gRPC menjadi teks
    // Normalnya, kode yang memanggil service RPC yang harus bertanggung jawab untuk
    //  menangkap error dengan `try-catch`/`Promise.catch()`, lalu melakukan
    //  error handling sesuai flow bisnis.
    if (err.code in GRPC_ERROR_CODE_MAP)
        resp.status(400).json({
            errorCode: err.code + ': ' + GRPC_ERROR_CODE_MAP[err.code],
            errorMessage: err.details,
        });
    // Error non-gRPC, keluarkan error default Express
    else next(err);
});

app.listen(8080);
