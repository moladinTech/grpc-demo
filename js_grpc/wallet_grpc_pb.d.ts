// package: moongirl.protodemo.v1
// file: wallet.proto

/* tslint:disable */
/* eslint-disable */

import * as grpc from "grpc";
import * as wallet_pb from "./wallet_pb";

interface IWalletService extends grpc.ServiceDefinition<grpc.UntypedServiceImplementation> {
    activateAccount: IWalletService_IActivateAccount;
    getBalance: IWalletService_IGetBalance;
    deposit: IWalletService_IDeposit;
    withdraw: IWalletService_IWithdraw;
    transfer: IWalletService_ITransfer;
}

interface IWalletService_IActivateAccount extends grpc.MethodDefinition<wallet_pb.ActivateAccountRequest, wallet_pb.ActivateAccountResponse> {
    path: "/moongirl.protodemo.v1.Wallet/ActivateAccount";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<wallet_pb.ActivateAccountRequest>;
    requestDeserialize: grpc.deserialize<wallet_pb.ActivateAccountRequest>;
    responseSerialize: grpc.serialize<wallet_pb.ActivateAccountResponse>;
    responseDeserialize: grpc.deserialize<wallet_pb.ActivateAccountResponse>;
}
interface IWalletService_IGetBalance extends grpc.MethodDefinition<wallet_pb.GetBalanceRequest, wallet_pb.GetBalanceResponse> {
    path: "/moongirl.protodemo.v1.Wallet/GetBalance";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<wallet_pb.GetBalanceRequest>;
    requestDeserialize: grpc.deserialize<wallet_pb.GetBalanceRequest>;
    responseSerialize: grpc.serialize<wallet_pb.GetBalanceResponse>;
    responseDeserialize: grpc.deserialize<wallet_pb.GetBalanceResponse>;
}
interface IWalletService_IDeposit extends grpc.MethodDefinition<wallet_pb.DepositRequest, wallet_pb.DepositResponse> {
    path: "/moongirl.protodemo.v1.Wallet/Deposit";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<wallet_pb.DepositRequest>;
    requestDeserialize: grpc.deserialize<wallet_pb.DepositRequest>;
    responseSerialize: grpc.serialize<wallet_pb.DepositResponse>;
    responseDeserialize: grpc.deserialize<wallet_pb.DepositResponse>;
}
interface IWalletService_IWithdraw extends grpc.MethodDefinition<wallet_pb.WithdrawalRequest, wallet_pb.WithdrawalResponse> {
    path: "/moongirl.protodemo.v1.Wallet/Withdraw";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<wallet_pb.WithdrawalRequest>;
    requestDeserialize: grpc.deserialize<wallet_pb.WithdrawalRequest>;
    responseSerialize: grpc.serialize<wallet_pb.WithdrawalResponse>;
    responseDeserialize: grpc.deserialize<wallet_pb.WithdrawalResponse>;
}
interface IWalletService_ITransfer extends grpc.MethodDefinition<wallet_pb.TransferRequest, wallet_pb.TransferResponse> {
    path: "/moongirl.protodemo.v1.Wallet/Transfer";
    requestStream: false;
    responseStream: false;
    requestSerialize: grpc.serialize<wallet_pb.TransferRequest>;
    requestDeserialize: grpc.deserialize<wallet_pb.TransferRequest>;
    responseSerialize: grpc.serialize<wallet_pb.TransferResponse>;
    responseDeserialize: grpc.deserialize<wallet_pb.TransferResponse>;
}

export const WalletService: IWalletService;

export interface IWalletServer {
    activateAccount: grpc.handleUnaryCall<wallet_pb.ActivateAccountRequest, wallet_pb.ActivateAccountResponse>;
    getBalance: grpc.handleUnaryCall<wallet_pb.GetBalanceRequest, wallet_pb.GetBalanceResponse>;
    deposit: grpc.handleUnaryCall<wallet_pb.DepositRequest, wallet_pb.DepositResponse>;
    withdraw: grpc.handleUnaryCall<wallet_pb.WithdrawalRequest, wallet_pb.WithdrawalResponse>;
    transfer: grpc.handleUnaryCall<wallet_pb.TransferRequest, wallet_pb.TransferResponse>;
}

export interface IWalletClient {
    activateAccount(request: wallet_pb.ActivateAccountRequest, callback: (error: grpc.ServiceError | null, response: wallet_pb.ActivateAccountResponse) => void): grpc.ClientUnaryCall;
    activateAccount(request: wallet_pb.ActivateAccountRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: wallet_pb.ActivateAccountResponse) => void): grpc.ClientUnaryCall;
    activateAccount(request: wallet_pb.ActivateAccountRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: wallet_pb.ActivateAccountResponse) => void): grpc.ClientUnaryCall;
    getBalance(request: wallet_pb.GetBalanceRequest, callback: (error: grpc.ServiceError | null, response: wallet_pb.GetBalanceResponse) => void): grpc.ClientUnaryCall;
    getBalance(request: wallet_pb.GetBalanceRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: wallet_pb.GetBalanceResponse) => void): grpc.ClientUnaryCall;
    getBalance(request: wallet_pb.GetBalanceRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: wallet_pb.GetBalanceResponse) => void): grpc.ClientUnaryCall;
    deposit(request: wallet_pb.DepositRequest, callback: (error: grpc.ServiceError | null, response: wallet_pb.DepositResponse) => void): grpc.ClientUnaryCall;
    deposit(request: wallet_pb.DepositRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: wallet_pb.DepositResponse) => void): grpc.ClientUnaryCall;
    deposit(request: wallet_pb.DepositRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: wallet_pb.DepositResponse) => void): grpc.ClientUnaryCall;
    withdraw(request: wallet_pb.WithdrawalRequest, callback: (error: grpc.ServiceError | null, response: wallet_pb.WithdrawalResponse) => void): grpc.ClientUnaryCall;
    withdraw(request: wallet_pb.WithdrawalRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: wallet_pb.WithdrawalResponse) => void): grpc.ClientUnaryCall;
    withdraw(request: wallet_pb.WithdrawalRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: wallet_pb.WithdrawalResponse) => void): grpc.ClientUnaryCall;
    transfer(request: wallet_pb.TransferRequest, callback: (error: grpc.ServiceError | null, response: wallet_pb.TransferResponse) => void): grpc.ClientUnaryCall;
    transfer(request: wallet_pb.TransferRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: wallet_pb.TransferResponse) => void): grpc.ClientUnaryCall;
    transfer(request: wallet_pb.TransferRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: wallet_pb.TransferResponse) => void): grpc.ClientUnaryCall;
}

export class WalletClient extends grpc.Client implements IWalletClient {
    constructor(address: string, credentials: grpc.ChannelCredentials, options?: object);
    public activateAccount(request: wallet_pb.ActivateAccountRequest, callback: (error: grpc.ServiceError | null, response: wallet_pb.ActivateAccountResponse) => void): grpc.ClientUnaryCall;
    public activateAccount(request: wallet_pb.ActivateAccountRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: wallet_pb.ActivateAccountResponse) => void): grpc.ClientUnaryCall;
    public activateAccount(request: wallet_pb.ActivateAccountRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: wallet_pb.ActivateAccountResponse) => void): grpc.ClientUnaryCall;
    public getBalance(request: wallet_pb.GetBalanceRequest, callback: (error: grpc.ServiceError | null, response: wallet_pb.GetBalanceResponse) => void): grpc.ClientUnaryCall;
    public getBalance(request: wallet_pb.GetBalanceRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: wallet_pb.GetBalanceResponse) => void): grpc.ClientUnaryCall;
    public getBalance(request: wallet_pb.GetBalanceRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: wallet_pb.GetBalanceResponse) => void): grpc.ClientUnaryCall;
    public deposit(request: wallet_pb.DepositRequest, callback: (error: grpc.ServiceError | null, response: wallet_pb.DepositResponse) => void): grpc.ClientUnaryCall;
    public deposit(request: wallet_pb.DepositRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: wallet_pb.DepositResponse) => void): grpc.ClientUnaryCall;
    public deposit(request: wallet_pb.DepositRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: wallet_pb.DepositResponse) => void): grpc.ClientUnaryCall;
    public withdraw(request: wallet_pb.WithdrawalRequest, callback: (error: grpc.ServiceError | null, response: wallet_pb.WithdrawalResponse) => void): grpc.ClientUnaryCall;
    public withdraw(request: wallet_pb.WithdrawalRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: wallet_pb.WithdrawalResponse) => void): grpc.ClientUnaryCall;
    public withdraw(request: wallet_pb.WithdrawalRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: wallet_pb.WithdrawalResponse) => void): grpc.ClientUnaryCall;
    public transfer(request: wallet_pb.TransferRequest, callback: (error: grpc.ServiceError | null, response: wallet_pb.TransferResponse) => void): grpc.ClientUnaryCall;
    public transfer(request: wallet_pb.TransferRequest, metadata: grpc.Metadata, callback: (error: grpc.ServiceError | null, response: wallet_pb.TransferResponse) => void): grpc.ClientUnaryCall;
    public transfer(request: wallet_pb.TransferRequest, metadata: grpc.Metadata, options: Partial<grpc.CallOptions>, callback: (error: grpc.ServiceError | null, response: wallet_pb.TransferResponse) => void): grpc.ClientUnaryCall;
}
