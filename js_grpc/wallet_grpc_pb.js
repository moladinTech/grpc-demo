// GENERATED CODE -- DO NOT EDIT!

'use strict';
var grpc = require('@grpc/grpc-js');
var wallet_pb = require('./wallet_pb.js');

function serialize_moongirl_protodemo_v1_ActivateAccountRequest(arg) {
  if (!(arg instanceof wallet_pb.ActivateAccountRequest)) {
    throw new Error('Expected argument of type moongirl.protodemo.v1.ActivateAccountRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_moongirl_protodemo_v1_ActivateAccountRequest(buffer_arg) {
  return wallet_pb.ActivateAccountRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_moongirl_protodemo_v1_ActivateAccountResponse(arg) {
  if (!(arg instanceof wallet_pb.ActivateAccountResponse)) {
    throw new Error('Expected argument of type moongirl.protodemo.v1.ActivateAccountResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_moongirl_protodemo_v1_ActivateAccountResponse(buffer_arg) {
  return wallet_pb.ActivateAccountResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_moongirl_protodemo_v1_DepositRequest(arg) {
  if (!(arg instanceof wallet_pb.DepositRequest)) {
    throw new Error('Expected argument of type moongirl.protodemo.v1.DepositRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_moongirl_protodemo_v1_DepositRequest(buffer_arg) {
  return wallet_pb.DepositRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_moongirl_protodemo_v1_DepositResponse(arg) {
  if (!(arg instanceof wallet_pb.DepositResponse)) {
    throw new Error('Expected argument of type moongirl.protodemo.v1.DepositResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_moongirl_protodemo_v1_DepositResponse(buffer_arg) {
  return wallet_pb.DepositResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_moongirl_protodemo_v1_GetBalanceRequest(arg) {
  if (!(arg instanceof wallet_pb.GetBalanceRequest)) {
    throw new Error('Expected argument of type moongirl.protodemo.v1.GetBalanceRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_moongirl_protodemo_v1_GetBalanceRequest(buffer_arg) {
  return wallet_pb.GetBalanceRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_moongirl_protodemo_v1_GetBalanceResponse(arg) {
  if (!(arg instanceof wallet_pb.GetBalanceResponse)) {
    throw new Error('Expected argument of type moongirl.protodemo.v1.GetBalanceResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_moongirl_protodemo_v1_GetBalanceResponse(buffer_arg) {
  return wallet_pb.GetBalanceResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_moongirl_protodemo_v1_TransferRequest(arg) {
  if (!(arg instanceof wallet_pb.TransferRequest)) {
    throw new Error('Expected argument of type moongirl.protodemo.v1.TransferRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_moongirl_protodemo_v1_TransferRequest(buffer_arg) {
  return wallet_pb.TransferRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_moongirl_protodemo_v1_TransferResponse(arg) {
  if (!(arg instanceof wallet_pb.TransferResponse)) {
    throw new Error('Expected argument of type moongirl.protodemo.v1.TransferResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_moongirl_protodemo_v1_TransferResponse(buffer_arg) {
  return wallet_pb.TransferResponse.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_moongirl_protodemo_v1_WithdrawalRequest(arg) {
  if (!(arg instanceof wallet_pb.WithdrawalRequest)) {
    throw new Error('Expected argument of type moongirl.protodemo.v1.WithdrawalRequest');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_moongirl_protodemo_v1_WithdrawalRequest(buffer_arg) {
  return wallet_pb.WithdrawalRequest.deserializeBinary(new Uint8Array(buffer_arg));
}

function serialize_moongirl_protodemo_v1_WithdrawalResponse(arg) {
  if (!(arg instanceof wallet_pb.WithdrawalResponse)) {
    throw new Error('Expected argument of type moongirl.protodemo.v1.WithdrawalResponse');
  }
  return Buffer.from(arg.serializeBinary());
}

function deserialize_moongirl_protodemo_v1_WithdrawalResponse(buffer_arg) {
  return wallet_pb.WithdrawalResponse.deserializeBinary(new Uint8Array(buffer_arg));
}


var WalletService = exports.WalletService = {
  // Mengaktifkan akun dompet.
activateAccount: {
    path: '/moongirl.protodemo.v1.Wallet/ActivateAccount',
    requestStream: false,
    responseStream: false,
    requestType: wallet_pb.ActivateAccountRequest,
    responseType: wallet_pb.ActivateAccountResponse,
    requestSerialize: serialize_moongirl_protodemo_v1_ActivateAccountRequest,
    requestDeserialize: deserialize_moongirl_protodemo_v1_ActivateAccountRequest,
    responseSerialize: serialize_moongirl_protodemo_v1_ActivateAccountResponse,
    responseDeserialize: deserialize_moongirl_protodemo_v1_ActivateAccountResponse,
  },
  // Lihat saldo akun saat ini.
getBalance: {
    path: '/moongirl.protodemo.v1.Wallet/GetBalance',
    requestStream: false,
    responseStream: false,
    requestType: wallet_pb.GetBalanceRequest,
    responseType: wallet_pb.GetBalanceResponse,
    requestSerialize: serialize_moongirl_protodemo_v1_GetBalanceRequest,
    requestDeserialize: deserialize_moongirl_protodemo_v1_GetBalanceRequest,
    responseSerialize: serialize_moongirl_protodemo_v1_GetBalanceResponse,
    responseDeserialize: deserialize_moongirl_protodemo_v1_GetBalanceResponse,
  },
  // Tambah saldo sebanyak jumlah tertentu.
deposit: {
    path: '/moongirl.protodemo.v1.Wallet/Deposit',
    requestStream: false,
    responseStream: false,
    requestType: wallet_pb.DepositRequest,
    responseType: wallet_pb.DepositResponse,
    requestSerialize: serialize_moongirl_protodemo_v1_DepositRequest,
    requestDeserialize: deserialize_moongirl_protodemo_v1_DepositRequest,
    responseSerialize: serialize_moongirl_protodemo_v1_DepositResponse,
    responseDeserialize: deserialize_moongirl_protodemo_v1_DepositResponse,
  },
  // Kurangi saldo sebanyak jumlah tertentu.
withdraw: {
    path: '/moongirl.protodemo.v1.Wallet/Withdraw',
    requestStream: false,
    responseStream: false,
    requestType: wallet_pb.WithdrawalRequest,
    responseType: wallet_pb.WithdrawalResponse,
    requestSerialize: serialize_moongirl_protodemo_v1_WithdrawalRequest,
    requestDeserialize: deserialize_moongirl_protodemo_v1_WithdrawalRequest,
    responseSerialize: serialize_moongirl_protodemo_v1_WithdrawalResponse,
    responseDeserialize: deserialize_moongirl_protodemo_v1_WithdrawalResponse,
  },
  // Memindahkan saldo dari 1 akun ke akun lain.
transfer: {
    path: '/moongirl.protodemo.v1.Wallet/Transfer',
    requestStream: false,
    responseStream: false,
    requestType: wallet_pb.TransferRequest,
    responseType: wallet_pb.TransferResponse,
    requestSerialize: serialize_moongirl_protodemo_v1_TransferRequest,
    requestDeserialize: deserialize_moongirl_protodemo_v1_TransferRequest,
    responseSerialize: serialize_moongirl_protodemo_v1_TransferResponse,
    responseDeserialize: deserialize_moongirl_protodemo_v1_TransferResponse,
  },
};

exports.WalletClient = grpc.makeGenericClientConstructor(WalletService);
