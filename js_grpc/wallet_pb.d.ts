// package: moongirl.protodemo.v1
// file: wallet.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class ActivateAccountRequest extends jspb.Message { 
    getUsername(): string;
    setUsername(value: string): ActivateAccountRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ActivateAccountRequest.AsObject;
    static toObject(includeInstance: boolean, msg: ActivateAccountRequest): ActivateAccountRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ActivateAccountRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ActivateAccountRequest;
    static deserializeBinaryFromReader(message: ActivateAccountRequest, reader: jspb.BinaryReader): ActivateAccountRequest;
}

export namespace ActivateAccountRequest {
    export type AsObject = {
        username: string,
    }
}

export class ActivateAccountResponse extends jspb.Message { 

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): ActivateAccountResponse.AsObject;
    static toObject(includeInstance: boolean, msg: ActivateAccountResponse): ActivateAccountResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: ActivateAccountResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): ActivateAccountResponse;
    static deserializeBinaryFromReader(message: ActivateAccountResponse, reader: jspb.BinaryReader): ActivateAccountResponse;
}

export namespace ActivateAccountResponse {
    export type AsObject = {
    }
}

export class GetBalanceRequest extends jspb.Message { 
    getUsername(): string;
    setUsername(value: string): GetBalanceRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GetBalanceRequest.AsObject;
    static toObject(includeInstance: boolean, msg: GetBalanceRequest): GetBalanceRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GetBalanceRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GetBalanceRequest;
    static deserializeBinaryFromReader(message: GetBalanceRequest, reader: jspb.BinaryReader): GetBalanceRequest;
}

export namespace GetBalanceRequest {
    export type AsObject = {
        username: string,
    }
}

export class GetBalanceResponse extends jspb.Message { 
    getBalance(): number;
    setBalance(value: number): GetBalanceResponse;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): GetBalanceResponse.AsObject;
    static toObject(includeInstance: boolean, msg: GetBalanceResponse): GetBalanceResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: GetBalanceResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): GetBalanceResponse;
    static deserializeBinaryFromReader(message: GetBalanceResponse, reader: jspb.BinaryReader): GetBalanceResponse;
}

export namespace GetBalanceResponse {
    export type AsObject = {
        balance: number,
    }
}

export class DepositRequest extends jspb.Message { 
    getUsername(): string;
    setUsername(value: string): DepositRequest;
    getAmount(): number;
    setAmount(value: number): DepositRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): DepositRequest.AsObject;
    static toObject(includeInstance: boolean, msg: DepositRequest): DepositRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: DepositRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): DepositRequest;
    static deserializeBinaryFromReader(message: DepositRequest, reader: jspb.BinaryReader): DepositRequest;
}

export namespace DepositRequest {
    export type AsObject = {
        username: string,
        amount: number,
    }
}

export class DepositResponse extends jspb.Message { 
    getBalance(): number;
    setBalance(value: number): DepositResponse;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): DepositResponse.AsObject;
    static toObject(includeInstance: boolean, msg: DepositResponse): DepositResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: DepositResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): DepositResponse;
    static deserializeBinaryFromReader(message: DepositResponse, reader: jspb.BinaryReader): DepositResponse;
}

export namespace DepositResponse {
    export type AsObject = {
        balance: number,
    }
}

export class WithdrawalRequest extends jspb.Message { 
    getUsername(): string;
    setUsername(value: string): WithdrawalRequest;
    getAmount(): number;
    setAmount(value: number): WithdrawalRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): WithdrawalRequest.AsObject;
    static toObject(includeInstance: boolean, msg: WithdrawalRequest): WithdrawalRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: WithdrawalRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): WithdrawalRequest;
    static deserializeBinaryFromReader(message: WithdrawalRequest, reader: jspb.BinaryReader): WithdrawalRequest;
}

export namespace WithdrawalRequest {
    export type AsObject = {
        username: string,
        amount: number,
    }
}

export class WithdrawalResponse extends jspb.Message { 
    getBalance(): number;
    setBalance(value: number): WithdrawalResponse;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): WithdrawalResponse.AsObject;
    static toObject(includeInstance: boolean, msg: WithdrawalResponse): WithdrawalResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: WithdrawalResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): WithdrawalResponse;
    static deserializeBinaryFromReader(message: WithdrawalResponse, reader: jspb.BinaryReader): WithdrawalResponse;
}

export namespace WithdrawalResponse {
    export type AsObject = {
        balance: number,
    }
}

export class TransferRequest extends jspb.Message { 
    getSenderUsername(): string;
    setSenderUsername(value: string): TransferRequest;
    getRecipientUsername(): string;
    setRecipientUsername(value: string): TransferRequest;
    getAmount(): number;
    setAmount(value: number): TransferRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): TransferRequest.AsObject;
    static toObject(includeInstance: boolean, msg: TransferRequest): TransferRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: TransferRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): TransferRequest;
    static deserializeBinaryFromReader(message: TransferRequest, reader: jspb.BinaryReader): TransferRequest;
}

export namespace TransferRequest {
    export type AsObject = {
        senderUsername: string,
        recipientUsername: string,
        amount: number,
    }
}

export class TransferResponse extends jspb.Message { 
    getSenderBalance(): number;
    setSenderBalance(value: number): TransferResponse;
    getRecipientBalance(): number;
    setRecipientBalance(value: number): TransferResponse;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): TransferResponse.AsObject;
    static toObject(includeInstance: boolean, msg: TransferResponse): TransferResponse.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: TransferResponse, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): TransferResponse;
    static deserializeBinaryFromReader(message: TransferResponse, reader: jspb.BinaryReader): TransferResponse;
}

export namespace TransferResponse {
    export type AsObject = {
        senderBalance: number,
        recipientBalance: number,
    }
}
