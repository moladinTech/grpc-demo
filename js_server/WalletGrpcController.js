const grpc = require('@grpc/grpc-js');

const { grpcController } = require('./classDecorator');
const WalletService = require('./WalletService');
const walletMsg = require('../js_grpc/wallet_pb');

/**
 * Logika untuk menerjemahkan request gRPC ke layer service,
 * serta mengubah hasil eksekusi service ke respon gRPC.
 *
 * **NOTE**: samakan nama method dengan nama RPC di `.proto`.
 */
module.exports = grpcController(
    class WalletGrpcController {
        constructor() {
            this._walletService = new WalletService();
        }

        /**
         * @param {grpc.ServerUnaryCall<walletMsg.ActivateAccountRequest, walletMsg.ActivateAccountResponse>} call
         * @param {grpc.sendUnaryData<walletMsg.ActivateAccountRequest>} callback
         */
        activateAccount(call, callback) {
            const username = call.request.getUsername();
            this._walletService.activateAccount(username);

            callback(null, new walletMsg.ActivateAccountResponse());
        }

        /**
         * @param {grpc.ServerUnaryCall<walletMsg.GetBalanceRequest, walletMsg.GetBalanceResponse>} call
         * @param {grpc.sendUnaryData<walletMsg.GetBalanceRequest>} callback
         */
        getBalance(call, callback) {
            const username = call.request.getUsername();
            const balance = this._walletService.getBalance(username);

            callback(
                null,
                new walletMsg.GetBalanceResponse().setBalance(balance)
            );
        }

        /**
         * @param {grpc.ServerUnaryCall<walletMsg.DepositRequest, walletMsg.DepositResponse>} call
         * @param {grpc.sendUnaryData<walletMsg.DepositRequest>} callback
         */
        deposit(call, callback) {
            const username = call.request.getUsername();
            const amount = call.request.getAmount();
            const balance = this._walletService.deposit(username, amount);

            callback(null, new walletMsg.DepositResponse().setBalance(balance));
        }

        /**
         * @param {grpc.ServerUnaryCall<walletMsg.WithdrawRequest, walletMsg.WithdrawResponse>} call
         * @param {grpc.sendUnaryData<walletMsg.WithdrawRequest>} callback
         */
        withdraw(call, callback) {
            const username = call.request.getUsername();
            const amount = call.request.getAmount();
            const balance = this._walletService.withdraw(username, amount);

            callback(
                null,
                new walletMsg.WithdrawalResponse().setBalance(balance)
            );
        }

        /**
         * @param {grpc.ServerUnaryCall<walletMsg.TransferRequest, walletMsg.TransferResponse>} call
         * @param {grpc.sendUnaryData<walletMsg.TransferRequest>} callback
         */
        transfer(call, callback) {
            const sender = call.request.getSenderUsername();
            const recipient = call.request.getRecipientUsername();
            const amount = call.request.getAmount();

            const balanceObj = this._walletService.transfer(
                sender,
                recipient,
                amount
            );

            callback(
                null,
                new walletMsg.TransferResponse()
                    .setSenderBalance(balanceObj.senderBalance)
                    .setRecipientBalance(balanceObj.recipientBalance)
            );
        }
    }
);
