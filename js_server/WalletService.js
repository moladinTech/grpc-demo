const {
    UsernameExistError,
    InvalidRequestError,
    UsernameNotExistError,
    InsufficientBalanceError,
} = require('./serverError');

/**
 * Logika bisnis yang berhubungan dengan _wallet_.
 */
module.exports = class WalletService {
    constructor() {
        /**
         * Tempat penyimpanan (dummy) data wallet.
         * @type {object<string, number>}
         */
        this._walletData = {};
    }

    /**
     * Mengaktifkan akun untuk username tertentu.
     *
     * @param {string} username Nama user yang _wallet_-nya akan diaktifkan.
     *
     * @throws {InvalidRequestError} bila username yang diberikan kosong.
     * @throws {UsernameExistError} bila aktifasi gagal karena username sudah terdaftar di sistem.
     */
    activateAccount(username) {
        console.log(`SERVER: mengatifkan akun ${username}`);

        const lowercaseUsername = this._validateAndLowercaseUsername(username);
        if (lowercaseUsername == null)
            throw new InvalidRequestError('username must not be empty');

        // Simpan username dalam huruf kecil
        if (lowercaseUsername in this._walletData)
            throw new UsernameExistError();

        // Buat akun wallet baru
        this._walletData[lowercaseUsername] = 0;
    }

    /**
     * Cari berapa saldo user saat ini.
     *
     * @param {string} username Nama user yang saldo _wallet_-nya dicari.
     *
     * @returns {number} Integer saldo user ini.
     *
     * @throws {InvalidRequestError} bila username yang diberikan kosong.
     * @throws {UsernameNotExistError} bila username tidak ditemukan di sistem.
     */
    getBalance(username) {
        console.log(`SERVER: ${username} mencari saldo walletnya`);

        const lowercaseUsername = this._validateAndLowercaseUsername(username);
        if (lowercaseUsername == null)
            throw new InvalidRequestError('username must not be empty');

        // Cek bila username belum terdaftar
        if (!(lowercaseUsername in this._walletData))
            throw new UsernameNotExistError();

        // Keluarkan saldo
        return this._walletData[lowercaseUsername];
    }

    /**
     * Menambah saldo _wallet_ 1 user.
     *
     * @param {string} username Nama user yang saldonya akan ditambah.
     * @param {number} amount Jumlah penambahan saldo.
     *
     * @returns {number} Saldo user setelah penambahan saldo.
     *
     * @throws {InvalidRequestError} bila username yang diberikan kosong.
     * @throws {InvalidRequestError} bila jumlah yang diberikan negatif.
     * @throws {UsernameNotExistError} bila username tidak ditemukan di sistem.
     */
    deposit(username, amount) {
        console.log(`SERVER: ${username} menyetor ${amount} ke walletnya`);

        const lowercaseUsername = this._validateAndLowercaseUsername(username);
        if (lowercaseUsername == null)
            throw new InvalidRequestError('username must not be empty');

        if (amount < 0)
            throw new InvalidRequestError('amount cannot be negative');

        // Cek bila username belum terdaftar
        if (!(lowercaseUsername in this._walletData))
            throw new UsernameNotExistError();

        // Keluarkan saldo setelah ditambah
        this._walletData[lowercaseUsername] += amount;
        return this._walletData[lowercaseUsername];
    }

    /**
     * Mengurangi saldo _wallet_ 1 user.
     *
     * @param {string} username Nama user yang saldonya akan dikurangi.
     * @param {number} amount Jumlah pengurangan saldo.
     *
     * @returns {number} Saldo user setelah pengurangan saldo.
     *
     * @throws {InvalidRequestError} bila username yang diberikan kosong.
     * @throws {InvalidRequestError} bila jumlah yang diberikan negatif.
     * @throws {UsernameNotExistError} bila username tidak ditemukan di sistem.
     * @throws {InsufficientBalanceError} bila saldo tidak mencukupi untuk penarikan.
     */
    withdraw(username, amount) {
        console.log(`SERVER: ${username} menarik ${amount} dari walletnya`);

        const lowercaseUsername = this._validateAndLowercaseUsername(username);
        if (lowercaseUsername == null)
            throw new InvalidRequestError('username must not be empty');

        if (amount < 0)
            throw new InvalidRequestError('amount cannot be negative');

        // Cek bila username belum terdaftar
        if (!(lowercaseUsername in this._walletData))
            throw new UsernameNotExistError();

        // Cek saldo sebelum dikurangi
        if (this._walletData[lowercaseUsername] < amount)
            throw new InsufficientBalanceError();

        // Keluarkan saldo setelah dikurangi
        this._walletData[lowercaseUsername] -= amount;
        return this._walletData[lowercaseUsername];
    }

    /**
     * Memindahkan saldo _wallet_  dari 1 user ke user lain.
     *
     * @param {string} senderUsername Nama user pengirim transfer.
     * @param {string} recipientUsername Nama user penerima transfer.
     * @param {number} amount Jumlah saldo yang ditransfer.
     *
     * @returns {{senderBalance: number, recipientBalance: number}} Saldo kedua user setelah transfer selesai.
     *
     * @throws {InvalidRequestError} bila ada username yang kosong.
     * @throws {InvalidRequestError} bila jumlah yang diberikan negatif.
     * @throws {UsernameNotExistError} bila ada username tidak ditemukan di sistem.
     * @throws {InsufficientBalanceError} bila saldo pengirim tidak cukup untuk transfer.
     */
    transfer(senderUsername, recipientUsername, amount) {
        console.log(
            `SERVER: transfer ${amount} dari ${senderUsername} ke ${recipientUsername}`
        );

        const lowercaseSenderUsername =
            this._validateAndLowercaseUsername(senderUsername);
        if (lowercaseSenderUsername == null)
            throw new InvalidRequestError('sender username must not be empty');

        const lowercaseRecipientUsername =
            this._validateAndLowercaseUsername(recipientUsername);
        if (lowercaseRecipientUsername == null)
            throw new InvalidRequestError(
                'recipient username must not be empty'
            );

        if (amount < 0)
            throw new InvalidRequestError('amount cannot be negative');

        // Cek bila ada username yang belum terdaftar
        if (!(lowercaseSenderUsername in this._walletData))
            throw new UsernameNotExistError('sender username does not exist');
        if (!(lowercaseRecipientUsername in this._walletData))
            throw new UsernameNotExistError(
                'recipient username does not exist'
            );

        // Cek saldo sebelum dikurangi
        if (this._walletData[lowercaseSenderUsername] < amount)
            throw new InsufficientBalanceError();

        // Transfer saldo
        this._walletData[lowercaseSenderUsername] -= amount;
        this._walletData[lowercaseRecipientUsername] += amount;

        return {
            senderBalance: this._walletData[lowercaseSenderUsername],
            recipientBalance: this._walletData[lowercaseRecipientUsername],
        };
    }

    ////////// PRIVATE METHOD
    /**
     * Validasi username yang diberikan, lalu mengeluarkan username versi huruf kecil.
     *
     * @param {string} username Teks yang akan divalidasi & dikecilkan hurufnya.
     *
     * @returns {string|null} Username versi huruf kecil, atau `null` bila validasi gagal.
     */
    _validateAndLowercaseUsername(username) {
        if (username == null || username.length === 0) return null;

        return ('' + username).toLowerCase();
    }
};
