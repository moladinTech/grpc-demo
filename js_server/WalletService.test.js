const WalletService = require('./WalletService');
const {
    UsernameExistError,
    InvalidRequestError,
    UsernameNotExistError,
    InsufficientBalanceError,
} = require('./serverError');

////////// activateAccount
describe('activateAccount', () => {
    it('sukses aktifasi akun', () => {
        // jika
        const tested = new WalletService();
        const username = 'akun baru';

        // ketika
        tested.activateAccount(username);

        // maka
        const balance = tested.getBalance(username);
        expect(balance).toEqual(0);
    });

    [
        {
            existingUsername: 'akun',
            newUsername: 'akun',
        },
        {
            existingUsername: 'akun_KAPITAL',
            newUsername: 'aKuN_KaPiTaL',
        },
        {
            existingUsername: '0',
            newUsername: 0,
        },
        {
            existingUsername: 'false',
            newUsername: false,
        },
        {
            existingUsername: 'nan',
            newUsername: NaN,
        },
    ].forEach((testObj) => {
        it(`gagal aktifasi karena username '${testObj.newUsername}' sudah ada`, () => {
            // jika
            const tested = new WalletService();
            tested.activateAccount(testObj.existingUsername);

            // ketika
            const errFunc = () => {
                tested.activateAccount(testObj.newUsername);
            };

            // maka
            expect(errFunc).toThrow(UsernameExistError);
        });
    });

    [
        {
            username: '',
            valueName: 'kosong',
        },
        {
            username: null,
            valueName: 'NULL',
        },
    ].forEach((testObj) => {
        it(`gagal aktifasi karena username ${testObj.valueName}`, () => {
            // jika
            const tested = new WalletService();

            // ketika
            const errFunc = () => {
                tested.activateAccount(testObj.username);
            };

            // maka
            expect(errFunc).toThrow(InvalidRequestError);
        });
    });
});

////////// getBalance
describe('getBalance', () => {
    it('sukses mendapatkan saldo', () => {
        // jika
        const tested = new WalletService();
        const username = 'akun saldo';
        tested.activateAccount(username);

        // ketika
        const balance = tested.getBalance(username);

        // maka
        expect(balance).toEqual(0);
    });

    it('gagal mendapatkan saldo karena username belum terdaftar', () => {
        // jika
        const tested = new WalletService();

        // ketika
        const errFunc = () => {
            tested.getBalance('akun invalid');
        };

        // maka
        expect(errFunc).toThrow(UsernameNotExistError);
    });

    [
        {
            username: '',
            valueName: 'kosong',
        },
        {
            username: null,
            valueName: 'NULL',
        },
    ].forEach((testObj) => {
        it(`gagal mendapatkan saldo karena username ${testObj.valueName}`, () => {
            // jika
            const tested = new WalletService();

            // ketika
            const errFunc = () => {
                tested.getBalance(testObj.username);
            };

            // maka
            expect(errFunc).toThrow(InvalidRequestError);
        });
    });
});

////////// deposit
describe('deposit', () => {
    it('sukses', () => {
        // jika
        const tested = new WalletService();
        const username = 'depositUser';
        const depositAmount = 1234;

        tested.activateAccount(username);

        // ketika
        const newBalance = tested.deposit(username, depositAmount);

        // maka
        expect(newBalance).toEqual(depositAmount);
        expect(tested.getBalance(username)).toEqual(depositAmount);
    });

    it('sukses dengan username beda kapital', () => {
        // jika
        const tested = new WalletService();
        const username = 'depositUser';
        const depositAmount = 1234;

        tested.activateAccount(username);

        // ketika
        const newBalance = tested.deposit(
            username.toUpperCase(),
            depositAmount
        );

        // maka
        expect(newBalance).toEqual(depositAmount);
        expect(tested.getBalance(username)).toEqual(depositAmount);
    });

    it('gagal karena user belum terdaftar', () => {
        // jika
        const tested = new WalletService();

        // ketika
        const errFunc = () => {
            tested.deposit('invalid', 1234);
        };

        // maka
        expect(errFunc).toThrowError(UsernameNotExistError);
    });

    it('gagal karena username kosong', () => {
        // jika
        const tested = new WalletService();

        // ketika
        const errFunc = () => {
            tested.deposit('', 1234);
        };

        // maka
        expect(errFunc).toThrowError(InvalidRequestError);
    });

    it('sukses dengan amount 0', () => {
        // jika
        const tested = new WalletService();
        const username = 'depositUser';

        tested.activateAccount(username);

        // ketika
        const newBalance = tested.deposit(username, 0);

        // maka
        expect(newBalance).toEqual(0);
        expect(tested.getBalance(username)).toEqual(0);
    });

    it('gagal karena angka negatif', () => {
        // jika
        const tested = new WalletService();
        const username = 'depositUser';

        tested.activateAccount(username);

        // ketika
        const errFunc = () => {
            tested.deposit(username, -111);
        };

        // maka
        expect(errFunc).toThrowError(InvalidRequestError);
    });
});

////////// withdraw
describe('withdraw', () => {
    [
        {
            initialBalance: 1234,
            withdrawnAmount: 1004,
            expectedRemainingBalance: 230,
        },
        {
            initialBalance: 123,
            withdrawnAmount: 123,
            expectedRemainingBalance: 0,
        },
    ].forEach((testObj) => {
        it(`sukses dengan sisa saldo ${testObj.expectedRemainingBalance}`, () => {
            // jika
            const tested = new WalletService();
            const username = 'withdrawUser';
            const withdrawAmount = testObj.withdrawnAmount;

            tested.activateAccount(username);
            tested.deposit(username, testObj.initialBalance);

            // ketika
            const newBalance = tested.withdraw(username, withdrawAmount);

            // maka
            expect(newBalance).toEqual(testObj.expectedRemainingBalance);
            expect(tested.getBalance(username)).toEqual(
                testObj.expectedRemainingBalance
            );
        });
    });

    it('sukses dengan username beda kapital', () => {
        // jika
        const tested = new WalletService();
        const username = 'withdrawUser';
        const withdrawAmount = 1004;

        tested.activateAccount(username);
        tested.deposit(username, 1234);

        // ketika
        const newBalance = tested.withdraw(
            username.toUpperCase(),
            withdrawAmount
        );

        // maka
        const expectedBalance = 230;
        expect(newBalance).toEqual(expectedBalance);
        expect(tested.getBalance(username)).toEqual(expectedBalance);
    });

    it('gagal karena user belum terdaftar', () => {
        // jika
        const tested = new WalletService();

        // ketika
        const errFunc = () => {
            tested.withdraw('invalid', 1234);
        };

        // maka
        expect(errFunc).toThrowError(UsernameNotExistError);
    });

    it('gagal karena username kosong', () => {
        // jika
        const tested = new WalletService();

        // ketika
        const errFunc = () => {
            tested.withdraw('', 1234);
        };

        // maka
        expect(errFunc).toThrowError(InvalidRequestError);
    });

    it('sukses dengan amount 0', () => {
        // jika
        const tested = new WalletService();
        const username = 'withdrawUser';

        tested.activateAccount(username);

        // ketika
        const newBalance = tested.withdraw(username, 0);

        // maka
        expect(newBalance).toEqual(0);
        expect(tested.getBalance(username)).toEqual(0);
    });

    it('gagal karena angka negatif', () => {
        // jika
        const tested = new WalletService();
        const username = 'withdrawUser';

        tested.activateAccount(username);

        // ketika
        const errFunc = () => {
            tested.withdraw(username, -111);
        };

        // maka
        expect(errFunc).toThrowError(InvalidRequestError);
    });

    it('gagal karena saldo tidak cukup', () => {
        // jika
        const tested = new WalletService();
        const username = 'withdrawUser';
        const initialBalance = 1234;

        tested.activateAccount(username);
        tested.deposit(username, initialBalance);

        // ketika
        const errFunc = () => {
            tested.withdraw(username, 10000);
        };

        // maka
        expect(errFunc).toThrowError(InsufficientBalanceError);
        expect(tested.getBalance(username)).toEqual(initialBalance);
    });
});

////////// transfer
describe('transfer', () => {
    [
        {
            initialBalance: 1234,
            transferAmount: 1004,
            expectedRemainingBalance: 230,
        },
        {
            initialBalance: 123,
            transferAmount: 123,
            expectedRemainingBalance: 0,
        },
    ].forEach((testObj) => {
        it(`sukses dengan sisa saldo pengirim ${testObj.expectedRemainingBalance}`, () => {
            // jika
            const tested = new WalletService();
            const senderUsername = 'senderUsername';
            const recipientUsername = 'recipientUsername';
            const transferAmount = testObj.transferAmount;

            tested.activateAccount(senderUsername);
            tested.activateAccount(recipientUsername);
            tested.deposit(senderUsername, testObj.initialBalance);

            // ketika
            const newBalanceObj = tested.transfer(
                senderUsername,
                recipientUsername,
                transferAmount
            );

            // maka
            expect(newBalanceObj.senderBalance).toEqual(
                testObj.expectedRemainingBalance
            );
            expect(tested.getBalance(senderUsername)).toEqual(
                testObj.expectedRemainingBalance
            );

            expect(newBalanceObj.recipientBalance).toEqual(transferAmount);
            expect(tested.getBalance(recipientUsername)).toEqual(
                transferAmount
            );
        });
    });

    it('sukses dengan username beda kapital', () => {
        // jika
        const tested = new WalletService();
        const senderUsername = 'senderUsername';
        const recipientUsername = 'recipientUsername';
        const transferAmount = 1200;

        tested.activateAccount(senderUsername);
        tested.activateAccount(recipientUsername);
        tested.deposit(senderUsername, 1234);

        // ketika
        const newBalanceObj = tested.transfer(
            senderUsername.toUpperCase(),
            recipientUsername.toUpperCase(),
            transferAmount
        );

        // maka
        const expectedRemainingBalance = 34;

        expect(newBalanceObj.senderBalance).toEqual(expectedRemainingBalance);
        expect(tested.getBalance(senderUsername)).toEqual(
            expectedRemainingBalance
        );

        expect(newBalanceObj.recipientBalance).toEqual(transferAmount);
        expect(tested.getBalance(recipientUsername)).toEqual(transferAmount);
    });

    it('gagal karena username pengirim belum terdaftar', () => {
        // jika
        const tested = new WalletService();
        const recipientUsername = 'recipientUsername';
        const unchangedBalance = 1235;

        tested.activateAccount(recipientUsername);
        tested.deposit(recipientUsername, unchangedBalance);

        // ketika
        const errFunc = () => {
            tested.transfer('invalid', recipientUsername, 1000);
        };

        // maka
        expect(errFunc).toThrowError(UsernameNotExistError);
        // Saldo penerima tidak boleh berubah
        expect(tested.getBalance(recipientUsername)).toEqual(unchangedBalance);
    });

    it('gagal karena username penerima belum terdaftar', () => {
        // jika
        const tested = new WalletService();
        const senderUsername = 'senderUsername';
        const unchangedBalance = 1235;

        tested.activateAccount(senderUsername);
        tested.deposit(senderUsername, unchangedBalance);

        // ketika
        const errFunc = () => {
            tested.transfer(senderUsername, 'invalid', 1000);
        };

        // maka
        expect(errFunc).toThrowError(UsernameNotExistError);
        // Saldo pengirim tidak boleh berubah
        expect(tested.getBalance(senderUsername)).toEqual(unchangedBalance);
    });

    it('gagal karena username pengirim kosong', () => {
        // jika
        const tested = new WalletService();
        const recipientUsername = 'recipientUsername';
        const unchangedBalance = 1235;

        tested.activateAccount(recipientUsername);
        tested.deposit(recipientUsername, unchangedBalance);

        // ketika
        const errFunc = () => {
            tested.transfer('', recipientUsername, 1000);
        };

        // maka
        expect(errFunc).toThrowError(InvalidRequestError);
        // Saldo penerima tidak boleh berubah
        expect(tested.getBalance(recipientUsername)).toEqual(unchangedBalance);
    });

    it('gagal karena username penerima kosong', () => {
        // jika
        const tested = new WalletService();
        const senderUsername = 'senderUsername';
        const unchangedBalance = 1235;

        tested.activateAccount(senderUsername);
        tested.deposit(senderUsername, unchangedBalance);

        // ketika
        const errFunc = () => {
            tested.transfer(senderUsername, '', 1000);
        };

        // maka
        expect(errFunc).toThrowError(InvalidRequestError);
        // Saldo pengirim tidak boleh berubah
        expect(tested.getBalance(senderUsername)).toEqual(unchangedBalance);
    });

    it('sukses dengan amount 0', () => {
        // jika
        const tested = new WalletService();
        const senderUsername = 'senderUsername';
        const recipientUsername = 'recipientUsername';

        const initSenderBalance = 1234;
        const initRecipientBalance = 4563;

        tested.activateAccount(senderUsername);
        tested.activateAccount(recipientUsername);
        tested.deposit(senderUsername, initSenderBalance);
        tested.deposit(recipientUsername, initRecipientBalance);

        // ketika
        const newBalanceObj = tested.transfer(
            senderUsername,
            recipientUsername,
            0
        );

        // maka
        expect(newBalanceObj.senderBalance).toEqual(initSenderBalance);
        expect(tested.getBalance(senderUsername)).toEqual(initSenderBalance);

        expect(newBalanceObj.recipientBalance).toEqual(initRecipientBalance);
        expect(tested.getBalance(recipientUsername)).toEqual(
            initRecipientBalance
        );
    });

    it('gagal karena angka negatif', () => {
        // jika
        const tested = new WalletService();
        const senderUsername = 'senderUsername';
        const recipientUsername = 'recipientUsername';

        const initSenderBalance = 1234;
        const initRecipientBalance = 4563;

        tested.activateAccount(senderUsername);
        tested.activateAccount(recipientUsername);
        tested.deposit(senderUsername, initSenderBalance);
        tested.deposit(recipientUsername, initRecipientBalance);

        // ketika
        const errFunc = () => {
            tested.transfer(senderUsername, recipientUsername, -100);
        };

        // maka
        expect(errFunc).toThrowError(InvalidRequestError);

        // Pastikan balance user tidak berubah
        expect(tested.getBalance(senderUsername)).toEqual(initSenderBalance);
        expect(tested.getBalance(recipientUsername)).toEqual(
            initRecipientBalance
        );
    });

    it('gagal karena saldo pengirim tidak cukup', () => {
        // jika
        const tested = new WalletService();
        const senderUsername = 'senderUsername';
        const recipientUsername = 'recipientUsername';

        const initSenderBalance = 1234;
        const initRecipientBalance = 4563;

        tested.activateAccount(senderUsername);
        tested.activateAccount(recipientUsername);
        tested.deposit(senderUsername, initSenderBalance);
        tested.deposit(recipientUsername, initRecipientBalance);

        // ketika
        const errFunc = () => {
            tested.transfer(senderUsername, recipientUsername, 99999);
        };

        // maka
        expect(errFunc).toThrowError(InsufficientBalanceError);

        // Pastikan balance user tidak berubah
        expect(tested.getBalance(senderUsername)).toEqual(initSenderBalance);
        expect(tested.getBalance(recipientUsername)).toEqual(
            initRecipientBalance
        );
    });
});
