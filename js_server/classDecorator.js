const grpc = require('@grpc/grpc-js');
const { GrpcCompatibleError } = require('./serverError');

/**
 * Kumpulan decorator (semacam middleware) yang dapat dipasang di kelas JS.
 */
module.exports = {
    /**
     * _Decorator_ untuk kelas controller yang berhubungan dengan gRPC.
     * Semua method yang ada di dalam kelas controller akan di-decorate dengan
     * logika tambahan yang berhubungan dengan gRPC.
     *
     * **NOTE**: Fungsi ini memiliki _side effect_: kelas yang diberikan sebagai argumen
     * akan dimodifikasi.
     *
     * @template CLS
     *
     * @param {CLS} grpcControllerClass Kelas yang method-methodnya akan di-_decorate_.
     *
     * @returns {CLS} Kelas yang method-nya sudah di di-_decorate_.
     */
    grpcController: function (grpcControllerClass) {
        const clsPrototype = grpcControllerClass.prototype;

        // Iterate method-method yang ada di dalam kelas
        for (const methodName of Object.getOwnPropertyNames(clsPrototype)) {
            // Lewati constructor & attribute yang bukan function
            if (
                methodName === 'constructor' ||
                typeof clsPrototype[methodName] !== 'function'
            )
                continue;

            const originalMethod = clsPrototype[methodName];
            const decoratedMethod = async function (call, callback) {
                try {
                    await originalMethod.call(this, call, callback);
                } catch (e) {
                    // Coba ubah error menjadi respon error gRPC
                    if (e instanceof GrpcCompatibleError) {
                        callback(e.toGrpcError());
                    } else {
                        // Error JS normal
                        console.error(e);
                        callback({
                            code: grpc.status.INTERNAL,
                            message: `internal error: ${e}`,
                        });
                    }
                }
            };

            // Pasang method versi baru
            clsPrototype[methodName] = decoratedMethod;
        }

        return grpcControllerClass;
    },
};
