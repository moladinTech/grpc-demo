const grpc = require('@grpc/grpc-js');

const walletSvc = require('../js_grpc/wallet_grpc_pb');
const WalletGrpcController = require('./WalletGrpcController');

(function main() {
    const server = new grpc.Server();
    const walletController = new WalletGrpcController();

    // Pasang service dari .proto ke server
    server.addService(walletSvc.WalletService, walletController);

    // Mulai jalankan server
    server.bindAsync(
        '0.0.0.0:50051',
        grpc.ServerCredentials.createInsecure(),
        () => {
            server.start();
        }
    );
})();
