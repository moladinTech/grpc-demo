const grpc = require('@grpc/grpc-js');

/**
 * Kelas error yang dapat diubah menjadi error gRPC.
 */
class GrpcCompatibleError extends Error {
    /**
     * @param {grpc.status} grpcStatus Status error gRPC untuk error ini.
     * @param {string} message Pesan error.
     * @param {Object<string,string>} [metadata] Info error tambahan, dalam bentuk `key: value`.
     */
    constructor(grpcStatus, message, metadata) {
        super(message);

        this.grpcStatus = grpcStatus;
        this.metadata = metadata;
    }

    /**
     * Ubah error ini menjadi bentuk error yang dapat dikirim lewat gRPC.
     *
     * @returns {grpc.StatusObject}
     */
    toGrpcError() {
        // Ubah metadata JS ke versi gRPC
        const grpcMetadata = new grpc.Metadata();
        if (this.metadata) {
            for (const metaKey in Object.keys(this.metadata)) {
                grpcMetadata.set(metaKey, this.metadata[metaKey]);
            }
        }

        return {
            code: this.grpcStatus,
            message: this.message,
            metadata: grpcMetadata,
        };
    }
}

/**
 * Kumpulan kelas error yang dapat diubah menjadi error gRPC.
 */
module.exports = {
    GrpcCompatibleError,

    /**
     * Error yang terjadi karena ada data yang salah/kurang di request.
     */
    InvalidRequestError: class extends GrpcCompatibleError {
        /**
         * @param {string} [message] Pesan error.
         * @param {Object<string,string>} [metadata] Info error tambahan, dalam bentuk `key: value`.
         */
        constructor(message, metadata) {
            super(
                grpc.status.INVALID_ARGUMENT,
                message || 'invalid request',
                metadata
            );
        }
    },

    /**
     * Error yang terjadi karena username sudah ada di sistem.
     */
    UsernameExistError: class extends GrpcCompatibleError {
        /**
         * @param {string} [message] Pesan error.
         * @param {Object<string,string>} [metadata] Info error tambahan, dalam bentuk `key: value`.
         */
        constructor(message, metadata) {
            super(
                grpc.status.ALREADY_EXISTS,
                message || 'username already exists',
                metadata
            );
        }
    },

    /**
     * Error yang terjadi karena username tidak ada di sistem.
     */
    UsernameNotExistError: class extends GrpcCompatibleError {
        /**
         * @param {string} [message] Pesan error.
         * @param {Object<string,string>} [metadata] Info error tambahan, dalam bentuk `key: value`.
         */
        constructor(message, metadata) {
            super(
                grpc.status.NOT_FOUND,
                message || 'username does not exist',
                metadata
            );
        }
    },

    /**
     * Error yang terjadi karena saldo di _wallet_ tidak cukup.
     */
    InsufficientBalanceError: class extends GrpcCompatibleError {
        /**
         * @param {string} [message] Pesan error.
         * @param {Object<string,string>} [metadata] Info error tambahan, dalam bentuk `key: value`.
         */
        constructor(message, metadata) {
            super(
                grpc.status.FAILED_PRECONDITION,
                message || 'insufficient balance',
                metadata
            );
        }
    },
};
